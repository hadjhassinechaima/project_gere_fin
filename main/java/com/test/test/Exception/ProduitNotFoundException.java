package com.test.test.Exception;

public class ProduitNotFoundException extends RuntimeException {
    public ProduitNotFoundException(String message){super(message);}
}
