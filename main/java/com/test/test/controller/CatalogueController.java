package com.test.test.controller;

import com.test.test.model.Catalogue;
import com.test.test.model.Category;
import com.test.test.model.Produit;
import com.test.test.repository.CatalogueRepository;
import com.test.test.repository.CategoryRepository;
import com.test.test.service.CatalogueService;
import com.test.test.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Catalogue")
@CrossOrigin(origins ="*")

public class CatalogueController {

    private final  CatalogueService service;
    private final  CatalogueRepository  Req;

    public CatalogueController( CatalogueRepository Req,  CatalogueService service) {
        this.Req =Req;
        this.service = service;
    }
    @GetMapping("/liste")
    public ResponseEntity<List<Catalogue>> getAllCategory() {
        List<Catalogue> C = service.findAllCatalogue();
        return new ResponseEntity<>(C , HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Catalogue> addCatalogue (@RequestBody Catalogue Ca) {

        Catalogue C = service.addCatalogue(Ca);
        return new ResponseEntity<>(C, HttpStatus.CREATED);
    }
   @DeleteMapping("/delete/{idCatalogue}")
    public void deleteCatalogue(@PathVariable("idCatalogue")Long idCatalogue)
    {
        this.service.deleteCatalogue(idCatalogue);
    }
    @PutMapping("/update/{idCatalogue}")
    public ResponseEntity<Object> updateCatalogue (@RequestBody Catalogue Cat , @PathVariable("idCatalogue") Long idCatalogue) {
        Optional<Catalogue> CatalogueOptional = Req.findById(idCatalogue);
        if (CatalogueOptional.isEmpty())
            return ResponseEntity.notFound().build();
        Cat.setIdCatalogue(idCatalogue);
        Req.save(Cat);
        return ResponseEntity.noContent().build();
    }
}
