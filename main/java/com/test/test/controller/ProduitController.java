package com.test.test.controller;


import com.test.test.model.Produit;
import com.test.test.repository.ProduitRepository;
import com.test.test.service.ProduitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/Produit")
@CrossOrigin(origins ="*")

public class ProduitController {
    private final ProduitRepository repo;

        private final ProduitService service;

        public ProduitController(ProduitService service,ProduitRepository repo) {
            super();
            this.service = service;
            this.repo = repo;
        }

        @GetMapping("/liste")
        public ResponseEntity<List<Produit>> getAllProduits() {
            List<Produit> prods = service.findAllProduits();
            return new ResponseEntity<>(prods, HttpStatus.OK);
        }
    @PostMapping("/add")
    public ResponseEntity<Produit> addProduit(@RequestBody Produit prod) {

        Produit Pro = service.addProduit(prod);
        return new ResponseEntity<>(Pro, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idProduit}")
    public void deleteProduit(@PathVariable("idProduit")Long idProduit)
    {
        this.service.deleteProduit(idProduit);
    }


    @GetMapping("/find/{idProduit}")
    public ResponseEntity<Produit> getproduitByIdProduit (@PathVariable("idProduit") Long idProduit) {
        Produit prod = service.findProduitByIdProduit(idProduit);
        return new ResponseEntity<>(prod, HttpStatus.OK);
    }

    @GetMapping("/produits/count")
        public Long count() {

            return service.count();
        }
    @PutMapping("/update/{idProduit}")
    public ResponseEntity<Object> updateProduit(@RequestBody Produit produit ,@PathVariable("idProduit") Long idProduit) {
        Optional<Produit> ProduitOptional = repo.findById(idProduit);
        if (ProduitOptional.isEmpty())
            return ResponseEntity.notFound().build();
        produit.setIdProduit(idProduit);
        repo.save(produit);
        return ResponseEntity.noContent().build();
    }


}
