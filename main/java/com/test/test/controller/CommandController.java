package com.test.test.controller;

import com.test.test.model.Command;
import com.test.test.model.Produit;
import com.test.test.repository.CommandRepository;
import com.test.test.service.CommandService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Command")
@CrossOrigin(origins ="*")

public class CommandController {
    private final CommandRepository repo;

    private final CommandService service;

    public CommandController(CommandService service,CommandRepository repo) {
        super();
        this.service = service;
        this.repo = repo;
    }

    @GetMapping("/liste")
    public ResponseEntity<List<Command>> getAllCommand() {
        List<Command> com = service.findAllCommand();
        return new ResponseEntity<>(com, HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Command> addCommand (@RequestBody Command C) {

        Command Cat = service.addCommand(C);
        return new ResponseEntity<>(Cat, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idCommand}")
    public void deleteCommand(@PathVariable("idCommand")Long idCommand)
    {
        this.service.deleteCommand(idCommand);
    }

    @PutMapping("/update/{idCommand}")
    public ResponseEntity<Object> updateCommand(@RequestBody Command com , @PathVariable("idCommand") Long idCommand) {
        Optional<Command> CommandOptional = repo.findById(idCommand);
        if (CommandOptional.isEmpty())
            return ResponseEntity.notFound().build();
        com.setIdCommand(idCommand);
        repo.save(com);
        return ResponseEntity.noContent().build();
    }

}
