package com.test.test.controller;

import com.test.test.model.Category;
import com.test.test.model.Produit;
import com.test.test.repository.CategoryRepository;
import com.test.test.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Categorie")
@CrossOrigin(origins ="*")

public class CategoryController {
    private final CategoryRepository Req;

    private final CategoryService service_c;

    public CategoryController(CategoryRepository req, CategoryService service_c) {
        Req = req;
        this.service_c = service_c;
    }

    @GetMapping("/liste")
    public ResponseEntity<List<Category>> getAllCategory() {
        List<Category> C = service_c.findAllCategory();
        return new ResponseEntity<>(C , HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Category> addCategory (@RequestBody Category C) {

        Category Cat = service_c.addCategory(C);
        return new ResponseEntity<>(Cat, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete/{idCategory}")
    public void deleteCategory(@PathVariable("idCategory")Long idCategory)
    {
        this.service_c.deleteCategory(idCategory);
    }
    @PutMapping("/update/{idCategory}")
    public ResponseEntity<Object> updateCategory(@RequestBody Category category ,@PathVariable("idCategory") Long idCategory) {
        Optional<Category> CategoryOptional = Req.findById(idCategory);
        if (CategoryOptional.isEmpty())
            return ResponseEntity.notFound().build();
        category.setIdCategory(idCategory);
        Req.save(category);
        return ResponseEntity.noContent().build();
    }


}

