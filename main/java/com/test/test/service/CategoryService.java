package com.test.test.service;

import com.test.test.Exception.ProduitNotFoundException;
import com.test.test.model.Category;
import com.test.test.model.Produit;
import com.test.test.repository.CategoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional

public class CategoryService {
    private final CategoryRepository Req ;


    public CategoryService(CategoryRepository repo)
    {
        this.Req = repo;
    }

    public List<Category> findAllCategory() {
        return Req.findAll();
    }

    public Category addCategory(Category C) {
        return Req.save(C);
    }


    public void deleteCategory(Long idCategory){
        Req.deleteById(idCategory);
    }


}
