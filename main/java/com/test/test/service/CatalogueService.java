package com.test.test.service;

import com.test.test.model.Catalogue;
import com.test.test.model.Category;
import com.test.test.model.Produit;
import com.test.test.repository.CatalogueRepository;
import com.test.test.repository.ProduitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional

public class CatalogueService {
    private final CatalogueRepository repo;

    public CatalogueService(CatalogueRepository repo) {
        this.repo = repo;
    }
    public List<Catalogue> findAllCatalogue() {
        return repo.findAll();
    }

    public Catalogue addCatalogue(Catalogue C) {
        return repo.save(C);
    }


    public void deleteCatalogue(Long idCatalogue){
        repo.deleteById(idCatalogue);
    }


}
