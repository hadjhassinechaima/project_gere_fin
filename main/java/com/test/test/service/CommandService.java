package com.test.test.service;

import com.test.test.model.Category;
import com.test.test.model.Command;
import com.test.test.model.Produit;
import com.test.test.repository.CommandRepository;
import com.test.test.repository.ProduitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CommandService {
    private final CommandRepository repo;

    public CommandService(CommandRepository repo) {
        this.repo = repo;
    }
    public List<Command> findAllCommand() {
        return repo.findAll();
    }

    public Command addCommand(Command C) {
        return repo.save(C);
    }


    public void deleteCommand(Long idCommand){
        repo.deleteById(idCommand);
    }


}
