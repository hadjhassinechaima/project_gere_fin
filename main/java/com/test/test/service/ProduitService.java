package com.test.test.service;



import com.test.test.Exception.ProduitNotFoundException;
import com.test.test.model.Produit;
import com.test.test.repository.ProduitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional

public class ProduitService {
    private final ProduitRepository repo;

    public ProduitService(ProduitRepository repo) {
        this.repo = repo;
    }
    public List<Produit> findAllProduits() {
        return repo.findAll();
    }

    public Produit addProduit(Produit prod) {

        return repo.save(prod);
    }


    public Long count() {
        return repo.count();
    }
    public Produit findProduitByIdProduit(Long idProduit) {
        return repo.findById(idProduit).orElseThrow(() -> new ProduitNotFoundException("Produit by id " + idProduit + " was not found"));
    }

    public void deleteProduit(Long idProduit){
        repo.deleteById(idProduit);
    }



}
