package com.test.test.repository;

import com.test.test.model.Command;
import com.test.test.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandRepository extends JpaRepository<Command, Long> {
    Command findCommandByIdCommand(Long idCommand);
}

