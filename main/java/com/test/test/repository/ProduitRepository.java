package com.test.test.repository;

import com.test.test.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProduitRepository extends JpaRepository<Produit, Long> {
    Produit findProduitByIdProduit(Long idProduit);
}
