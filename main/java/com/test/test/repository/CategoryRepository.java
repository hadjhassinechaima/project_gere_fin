package com.test.test.repository;

import com.test.test.model.Category;
import com.test.test.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findCategoryByIdCategory(Long idCategory);
}

