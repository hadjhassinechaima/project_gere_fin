package com.test.test.repository;

import com.test.test.model.Catalogue;
import com.test.test.model.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatalogueRepository extends JpaRepository<Catalogue, Long> {
    Catalogue findCatalogueByIdCatalogue(Long idCatalogue);
}
