package com.test.test.model;

import com.sun.istack.NotNull;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Column(name = "idC" ,length = 50)
    private Long idCategory;
    @NotNull
    private String titre;
    private String description;

@JsonIgnore

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    public Category() {
    }

    @Override
    public String toString() {
        return "Category{" +
                "idCategory=" + idCategory +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "category",cascade = {CascadeType.MERGE})
    private Set<Produit> pro;

}
