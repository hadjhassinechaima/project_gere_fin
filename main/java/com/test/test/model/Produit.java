package com.test.test.model;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProduit;
    private String titre;
    private String description;
    private double prix;
    private Date dateCreation;
    public Produit() {
        super();
    }

    @JsonIgnore()
    public Long getIdProduit() {
        return idProduit;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public double getPrix() {
        return prix;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;


    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }




    @Override
    public String toString() {
        return "Produit{" +
                 "idProduit=" + idProduit +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", prix=" + prix +
                ", dateCreation=" + dateCreation +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "produit",cascade = {CascadeType.MERGE})
    private Set<Lignecommand> LC;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idL")
    private  Category category;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idCT")
    private  Catalogue catalogue;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Catalogue getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(Catalogue catalogue) {
        this.catalogue = catalogue;
    }

}
