package com.test.test.model;

import javax.persistence.*;

@Entity
public class Lignecommand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int Qte;

    public int getQte() {
        return Qte;
    }

    public void setQte(int qte) {
        Qte = qte;
    }

    public Lignecommand() {

    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @ManyToOne(cascade = {CascadeType.MERGE})
   @JoinColumn(name = "idCom")
    private  Command command;
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idProd")
    private  Produit produit;

}
