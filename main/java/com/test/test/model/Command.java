package com.test.test.model;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCommand;
    private String nom;
    private String etat;
    private Double prix;

    public Long getIdCommand() {
        return idCommand;
    }

    public void setIdCommand(Long idCommand) {
        this.idCommand = idCommand;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Command() {
    }

    @Override
    public String toString() {
        return "Command{" +
                "idCommand=" + idCommand +
                ", nom='" + nom + '\'' +
                ", etat='" + etat + '\'' +
                ", prix=" + prix +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "command",cascade = {CascadeType.MERGE})
    private Set<Lignecommand> LC;

}
