package com.test.test.model;

import com.sun.istack.NotNull;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
public class Catalogue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCatalogue;
    @NotNull
    private String titre;
    private String description;

    private Date dateCreation;

    public Long getIdCatalogue() {
        return idCatalogue;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setIdCatalogue(Long idCatalogue) {
        this.idCatalogue = idCatalogue;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Catalogue() {

    }

    @Override
    public String toString() {
        return "catalogue{" +
                "idcatalogue=" + idCatalogue +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", dateCreation=" + dateCreation +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "catalogue",cascade = {CascadeType.MERGE})
    private Set<Produit> pro;

}
